---
title: "Comparaison des charges salariales SIASP et simulées"
author: "BPS/OSAM DREES - Équipe SMASH"
output:
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango

params:
  champs:
    label: "Champs des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]

geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"
---

# Présentation 

Ce script compare les charges totales employeurs calculées dans SMASH et déclarées
dans les données SIASP.

```{r generate_page, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer le document (copie sur I: en option)
# Vérifier également le chemin relatif vers le package SMASH, ci-dessus
out_format <- "html" # "pdf" # 
ch <- "hop"
rmd_file <- "Evaluation_Charges_Salariales"
out_local <-  sprintf("~/%s-%s.%s", rmd_file, ch, out_format)
  
# Génération au format demandé
rmarkdown::render(sprintf("%s.Rmd", rmd_file),
                  output_format = sprintf("%s_document", out_format),
                  output_file = out_local, 
                  params = list(champs = ch))
  
tryCatch( {
  # Tente une copie sur I:
  chemin_dist <- file.path(
    "I:/BPS/_Emplois et revenus",
    "Projet SMASH/03_Comptes_rendus/EtudesComportementales",
    "Evaluations")
  out_dist <- file.path(chemin_dist, basename(out_local))
  cat("Sauvegarde distante, dossier :", out_dist, "\n")
  file.copy(out_local, out_dist, overwrite = TRUE)
  },
  error = function(e) {
    message( 
      paste("sauvegarde distante impossible, fichier :", out_dist) )
  }
)

```



```{r librairies, include=FALSE}

library(plotly)

Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
devtools::load_all(config$path_smash)
chemin <- config$path_siasp

# Récupération paramètre année (entête Rmd)
anneeDebut <- 2010
anneeFin <- 2021
champs <- params$champs

v_annees <- as.character(seq(anneeDebut, anneeFin))

## Chargement des données 

# Chargement package Smash dev
fic_pop <- file.path(chemin, sprintf("popSiasp_%s_%d-%d.rds", champs, 
                                     anneeDebut, anneeFin) )
```



# Préparation des données

## Chargement population SIASP


```{r chargement_donnees, echo=FALSE}
popSIASP <- readRDS(fic_pop)
v_annees <- as.numeric(names(popSIASP))
```

Nous chargeons les données SIASP mises en forme au format SMASH entre les années
`r v_annees[1]` et `r v_annees[length(v_annees)]`.

## Calcul des charges patronnales

```{r calcul_charges, include=FALSE}
df_sum <- data.frame(an = NULL, 
                     STATUT_CONTRAT = NULL,
                     charges_siasp = NULL, 
                     charges_smash = NULL)

for (strAn in names(popSIASP)) {
  an <- as.numeric(strAn)
  if (!("TOT_COT_EMP_SIASP" %in% names(popSIASP[[strAn]]))) {
    # On renomme la variable déclarée dans SIASP (une seule fois)
    popSIASP[[strAn]]$TOT_COT_EMP_SIASP <- popSIASP[[strAn]]$TOT_COT_EMP
    
  }
  popSIASP[[strAn]] <- calculChargesEmployeurTotal(popSIASP[[strAn]], an)

  # Synthèse pour l'année par statut_contrat
  df_charges_an <- popSIASP[[strAn]] %>% group_by(STATUT_CONTRAT) %>% 
    summarize(charges_siasp=sum(TOT_COT_EMP_SIASP), 
              charges_smash=sum(TOT_COT_EMP), 
              S_BRUT = sum(S_BRUT), 
              EQTP = sum(EQTP)) %>%
    mutate(an = an)
              
  # Stockage
  df_sum <- rbind(df_sum, df_charges_an )
}

```

# Comparaison des charges

## Comparaison globale

Les figures ci-dessous comparent d'une part les valeurs absolues des charges 
par année en Milliards d'€ (Md€) et d'autre part les taux de cotisations 
patronales par année.

On voit que globalement : 

- Les charges patronales calculées par le simulateur DGOS sont autour de 50%
- Les charges déclarées dans les SIASP sont autour de 36%. Il y a également un 
problème apparent sur les données 2010/2011. 

```{r comparaison, echo=FALSE}

df_sum_all <- df_sum %>% group_by(an) %>% 
                 summarise(charges_siasp = sum(charges_siasp),
                           charges_smash = sum(charges_smash),
                           S_BRUT = sum(S_BRUT))

fig1 <- plot_ly(df_sum_all, 
               x = ~an, y = ~charges_siasp/1e9, 
               type = 'bar', name = 'SIASP') %>% 
  add_trace(y = ~charges_smash/1e9, name = 'SMASH') %>% 
  layout(title = "Comparaison des charges déclarées SIASP et calculées SMASH",
         yaxis = list(title = 'Md€'), barmode = 'group')


fig2 <- plot_ly(df_sum_all, x = ~an, y = ~100*charges_siasp/S_BRUT,
                type = 'bar', name = 'SIASP') %>% 
  add_trace(y = ~100*charges_smash/S_BRUT, name = 'SMASH') %>% 
  layout(title = "comparaison taux de charges SIASP Vs SMASH", 
         yaxis = list(title = '%', barmode = 'group'))
  

# Rendu HTML des figures créées précédemment
htmltools::tagList(list(fig1, fig2))
```

## Comparaison par statut


### Valeurs globales 

```{r comparaison_statut, echo=FALSE}
l_figs <- list()
l_statuts <- c("TITH", "CONT", "MEDI", "ELEV", "INTE", "CAID") 
# l_statuts <- unique(df_sum$STATUT_CONTRAT)

for (st in l_statuts) {
  l_figs[[st]] <- plot_ly(df_sum %>% filter(STATUT_CONTRAT==st), 
                          x = ~an, 
                          y = ~charges_siasp/1e9,
                          type = 'bar', name = 'SIASP') %>% 
    add_trace(y = ~charges_smash/1e9, name = 'SMASH') %>% 
    layout(title = paste("Statut", st, "comparaison charges SIASP Vs SMASH"), 
           yaxis = list(title = 'Md€'), barmode = 'group')
}


# Rendu HTML des figures créées précédemment
htmltools::tagList(l_figs)
```

### Taux de cotisations par statut

```{r comparaison_statut_taux, echo=FALSE}
l_figs <- list()
l_statuts <- c("TITH", "CONT", "MEDI", "ELEV", "INTE", "CAID") 
# l_statuts <- unique(df_sum$STATUT_CONTRAT)

for (st in l_statuts) {
  l_figs[[st]] <- plot_ly(df_sum %>% filter(STATUT_CONTRAT==st), 
                          x = ~an, 
                          y = ~100*charges_siasp/S_BRUT,
                          type = 'bar', name = 'SIASP') %>% 
    add_trace(y = ~100*charges_smash/S_BRUT, name = 'SMASH') %>% 
    layout(title = paste("Statut", st, 
                         ": taux de cotisations SIASP Vs SMASH"), 
           yaxis = list(title = '%', barmode = 'group'))
  
}


# Rendu HTML des figures créées précédemment
htmltools::tagList(l_figs)
```