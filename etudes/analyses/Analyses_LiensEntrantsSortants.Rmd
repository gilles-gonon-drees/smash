---
title: "Lien entre entrants et sortants SIASP `r params$annee`"
author: "DREES/OSAM/BPS - Équipe SMASH"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango

params:
  annee:
    label: "Année"
    value: 2018
    input: slider
    min: 2016
    max: 2019
    step: 1
    sep: ""
  champ:
    label: "Champ des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]

# indent: false 
# mainfont: Arial
# monofont: Arial
# fontsize: 12pt
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"

# abstract: \singlespacing BLABLABLA.
keyword: "SIASP"
---

```{r generate_models, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer l'ensemble des modèles+rapports 2016-2019
# Pour ne pas avoir de bloc de code : 
knitr::opts_chunk$set(echo = FALSE)

rmd_file <- "Analyses_LiensEntrantsSortants"

for (ch_ in c("hop", "fph")) {
  for (an_ in seq(2016,2019)) {
  
    out_local <- sprintf("~/%s_%s_%d.html", rmd_file, ch_, an_)
    rmarkdown::render(paste(rmd_file,".Rmd", sep = ""),
                      output_format = "html_document",
                      output_file = out_local, 
                      params = list(champ = ch_, annee = an_))
    
    cat("Année", an_, ", champ", ch_ , ": OK\n")
    
    tryCatch( {
      chemin_dist <- file.path("I:/BPS/_Emplois et revenus",
                               "Projet SMASH/03_Comptes_rendus",
                               "EtudesComportementales/AnalysesSurSIASP")
      
        cat("Sauvegarde distante, dossier :", chemin_dist, "\n")
        file.copy(out_local, chemin_dist, overwrite = TRUE)
       },
      error = function(e) {
        message( paste("sauvegarde distante impossible, dossier :", chemin_dist) )
      }
    )
    
  }
}


```


# Introduction 

## Objectifs

Le modèle d'entrant v2 existant permet de générer des entrants, mais il apparaît 
dans les simulations que la masse salariale diminue, ce qui provient sans doute 
du fait que les entrants ont un petit EQTP quand ils entrent, pour faire le 
complément des sortants et cet EQTP n'évolue pas la 2è année.

L'objet de ce notebook est donc d'étudier le lien entre les entrants et les 
sortants et de voir : 
- si les contrats de entrants succèdent aux contrats des entrants
- l'évolution du temps de travail des entrants l'année suivant leur embauche. 
- l'évolution des contrats des sortants l'année précédent leur sortie

Nous nous demanderons aussi quelle est la part de contrats courts dans les entrants, 
c'est à dire ceux qui rentrent une année et sortent l'année suivante.

```{r imports_et_options, include=FALSE}
library(patchwork)

## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)

# parametres globaux de graphiques 
ggplot2::theme_update(panel.background = element_rect(fill = "white"), 
                      panel.grid.major.y = element_line("grey"), 
                      panel.grid.major.x = element_blank(), 
                      legend.position = "bottom",
                      legend.title = ggplot2::element_blank(),
                      legend.text=element_text(size= 10), 
                      axis.text.y = element_text(size=10), 
                      axis.text.x = element_text(size=10), 
                      legend.background = element_rect(fill="white"), 
                      legend.key = element_rect(fill = "white"))

# Récupération  des paramètres
champ <- params$champ
annee <- params$annee
strAn <- as.character(annee)
strAnPrec <- as.character(annee-1)
strAnSuiv <- as.character(annee+1)

```

## Données étudiées

### Présentation

Dans cette étude nous travaillerons sur les personnels `r toupper(champ)` 
des années `r strAnPrec`, `r strAn`, `r strAnSuiv`.
afin de comparer d'une les entrants et sortants de `r strAn` et d'étudier l'évolution 
des entrants de `r strAn` en `r strAnSuiv`.

Les données chargées sont filtrées sur le critères `EQTP > 0`.

```{r chargement_donnees, message=FALSE, include=FALSE}

l_df <- list()
for (an in c(strAnPrec, strAn, strAnSuiv)) {
  l_df[[an]] <- chargerFST(champ, as.numeric(an), b_selection = TRUE, 
                           chemin = chemin, verbose = TRUE)
  l_df[[an]] <- l_df[[an]] %>% filter(EQTP >0)
}

```

Récupération des personnes à analyser : 

- les entrants en `r annee` (présents en `r annee`, non présents en `r annee-1`)
- les sortants après `r annee` (présents en `r annee`, non présents en `r annee+1`)
- les entrants de `r annee` toujours présents en `r annee+1`
- les entrants de `r annee` qui sortent la même année
- les sortants après `r annee` en `r annee-1` (présents en `r annee`, 
non présents en `r annee+1`)

```{r entrants_sortants, echo=FALSE}

idAnPrec <- unique(l_df[[strAnPrec]]$ID_NIR20)
idAn <- unique(l_df[[strAn]]$ID_NIR20)
idAnSuiv <- unique(l_df[[strAnSuiv]]$ID_NIR20)
id_in <- setdiff(idAn, idAnPrec)
id_out <- setdiff(idAn, idAnSuiv)
id_inout <- intersect(id_in, id_out)

# Entrants de l'année
df_in <- l_df[[strAn]] %>% filter(ID_NIR20 %in% id_in)
# Sortants de l'année (= non présents an suivant)
df_out <- l_df[[strAn]] %>% filter(ID_NIR20 %in% id_out)
# Entrants de An l'an suivant (pour voir ce qu'ils deviennent)
df_in_anSuiv <- l_df[[strAnSuiv]] %>% filter(ID_NIR20 %in% id_in)
# entrants de l'an qui sortent la même année
df_inout <- l_df[[strAn]] %>% filter(ID_NIR20 %in% id_inout)

# sortant après an présents l'année précédente (pour voir d'où ils viennent)
df_out_anPrec <- l_df[[strAnPrec]] %>% filter(ID_NIR20 %in% id_out)
id_out_anPrec <- unique(df_out_anPrec$ID_NIR20)

# Titularisations entrants anSuiv

nbTitulAnSuiv <- length(intersect(
  unique((df_in %>% filter(STATUT_CONTRAT=="CONT"))$ID_NIR20),
  unique((df_in_anSuiv %>% filter(STATUT_CONTRAT=="TITH"))$ID_NIR20)) )

```


```{r infos_entrants_sortants, results='asis', echo=FALSE}

cat(sprintf("\n### Infos effectifs **%d**, champs %s\n", annee, toupper(champ)))

cat(sprintf("\nEffectif globaux, champs %s : **%d**\n", toupper(champ), length(idAn)))


cat(sprintf("\nNombres d'entrants et sortants pour l'année %s :\n\n", strAn))


# Infos sortants
cat(sprintf("\n**%d sortants** en %s, dont :\n\n", length(id_out), strAn))

for (statut in c("TITH", "CONT", "MEDI")) {
  strInfos <- ifelse(
    statut=="CONT", 
    sprintf(", %d CDD, %d CDI", 
            length(unique((df_out %>% filter(STATUT_CONTRAT==statut & DUREE_CONTRAT=="CDD"))$ID_NIR20)),
            length(unique((df_out %>% filter(STATUT_CONTRAT==statut & DUREE_CONTRAT=="CDI"))$ID_NIR20))
            ), "")

  cat(sprintf(" - %d %s%s\n", 
              length(unique((df_out %>% filter(STATUT_CONTRAT==statut))$ID_NIR20)), 
              statut, strInfos) )
}

cat(sprintf(" - %d déjà présents en %s\n", length(unique(df_out_anPrec$ID_NIR20)), 
            strAnPrec))

nb_out_an_complet <- 
  length(unique((df_out %>% filter(DAT_FIN == as.POSIXlt(
    sprintf("%s-12-31", strAn), tz = "UTC")))$ID_NIR20) )
             
cat(sprintf(" - %d (%.1f%%) ont travaillé toute l'année (sortie au 31/12)\n", 
            nb_out_an_complet, 100*nb_out_an_complet/length(id_out)) )


cat(sprintf(" - %d entrants %s sortis en %s (restés moins d'un an)\n", 
            length(id_inout), strAn, strAn) )

# Infos entrants
cat(sprintf("\n**%d entrants** en %s, dont :\n\n", length(id_in), strAn))

for (statut in c("TITH", "CONT", "MEDI")) {
  strInfos <- ifelse(
    statut=="CONT", 
    sprintf(", %d CDD, %d CDI", 
            length(unique((df_in %>% filter(STATUT_CONTRAT==statut & DUREE_CONTRAT=="CDD"))$ID_NIR20)),
            length(unique((df_in %>% filter(STATUT_CONTRAT==statut & DUREE_CONTRAT=="CDI"))$ID_NIR20))
            ), "")

  cat(sprintf(" - %d %s%s\n", 
              length(unique((df_in %>% filter(STATUT_CONTRAT==statut))$ID_NIR20)), 
              statut, strInfos) )
}

cat(sprintf(" - %d toujours présents en %s, dont %d ont été titularisés (%.1f%% des CONT)\n",
            length(unique(df_in_anSuiv$ID_NIR20)),
            strAnSuiv, nbTitulAnSuiv, 
            100*nbTitulAnSuiv/length( 
              unique(df_in_anSuiv[df_in_anSuiv$STATUT_CONTRAT=="CONT",]$ID_NIR20))
            ) 
    )

nb_in_an_complet <- 
  length(unique((df_in %>% filter(DAT_DEBUT == as.POSIXlt(
    sprintf("%s-01-01", strAn), tz = "UTC")))$ID_NIR20) )


cat(sprintf(" - %d (%.1f%%) ont travaillé toute l'année (entrée au 01/01)\n", 
            nb_in_an_complet, 100*nb_in_an_complet/length(id_in)) )


# Infos présents-présents hors sortants (qui étaient présents l'an précédent)
idPP <- setdiff(intersect(idAn,idAnPrec), id_out)

cat(sprintf("\n**%d présents-présents** (hors sortants)\n\n", length(idPP)))

```

# Comparaison sortants / entrants

## Agrégat des postes 

On ne considère que les **titulaires** et les **contractuels**. 

Il est nécessaire d'agréger les postes par personne pour connaître la charge 
que chacun a effectué sur l'année. La procédure suivante est suivie : 

- Les sortants au 31/12 et les entrants au 01/01 sont supprimés des personnes 
analysées (i.e. les personnes ayant travaillées l'année complète)
- Les variables suivantes sommées, sont : `EQTP`, `S_BRUT`, `NB_HEURES_REMUN`, 
`DUREE_POSTE`
- Les informations du poste principal sont conservées :
(tri préalable sur `POSTE_PRINC_AN` croissant) : `SEXE`, `AGE`, `GRADE_ECH`, 
`CAT_ECH`, `QUOTITE`, `STATUT_CONTRAT`, `CORPS`, `SIREN`
- La `DAT_DEBUT` minimale est conservée
- La `DAT_FIN` maximale est conservée

```{r agregat_postes}
df_out_ag <- df_out %>% filter(STATUT_CONTRAT %in% c("TITH", "CONT")  & 
           DAT_FIN < as.POSIXlt(sprintf("%s-12-31", strAn), tz = "UTC") ) %>% 
  arrange(POSTE_PRINC_AN) %>% 
  group_by(ID_NIR20) %>% summarise(EQTP = sum(EQTP), 
                                   S_BRUT = sum(S_BRUT),
                                   PR_TOT_CAL = sum(PR_TOT_CAL),
                                   NB_HEURES_REMUN = sum(NB_HEURES_REMUN),
                                   DUREE_POSTE = sum(DUREE_POSTE),
                                   GRADE_ECH = first(GRADE_ECH),
                                   DAT_DEBUT = min(DAT_DEBUT),
                                   DAT_FIN = max(DAT_FIN),
                                   SEXE = first(SEXE), AGE = first(AGE),
                                   CAT_ECH = first(CAT_ECH),
                                   QUOTITE = first(QUOTITE),
                                   STATUT_CONTRAT = first(STATUT_CONTRAT),
                                   SIREN = first(SIREN),
                                   CORPS = first(CORPS),
                                   NB_POSTES = n(),
                                   .groups = "drop") %>%
  mutate(EQTP_COMPLEMENT = round(QUOTITE/100 - EQTP, digits = 2)) %>% 
  mutate(S_BRUT_COMPLEMENT = S_BRUT/EQTP * EQTP_COMPLEMENT )

df_in_ag <- df_in %>% filter(STATUT_CONTRAT %in% c("TITH", "CONT")  & 
           DAT_DEBUT > as.POSIXlt(sprintf("%s-01-01", strAn), tz = "UTC") ) %>% 
  arrange(POSTE_PRINC_AN) %>% 
  group_by(ID_NIR20) %>% summarise(EQTP = sum(EQTP), 
                                   S_BRUT = sum(S_BRUT),
                                   PR_TOT_CAL = sum(PR_TOT_CAL),
                                   NB_HEURES_REMUN = sum(NB_HEURES_REMUN),
                                   DUREE_POSTE = sum(DUREE_POSTE),
                                   GRADE_ECH = first(GRADE_ECH),
                                   DAT_DEBUT = min(DAT_DEBUT),
                                   DAT_FIN = max(DAT_FIN),
                                   SEXE = first(SEXE), AGE = first(AGE),
                                   CAT_ECH = first(CAT_ECH),
                                   QUOTITE = first(QUOTITE),
                                   STATUT_CONTRAT = first(STATUT_CONTRAT),
                                   SIREN = first(SIREN),
                                   CORPS = first(CORPS),
                                   NB_POSTES = n(),
                                   .groups = "drop")


```

Synthèse des nombres de postes agrégés pour les sortants et les entrants, 
périmètre restreint des TITH et CONT.

```{r agregat_nb_poste}
knitr::kable(merge.data.frame(
  df_in_ag %>% count(NB_POSTES, name = "n_entrants"),
  df_out_ag %>% count(NB_POSTES, name = "n_sortants"), all = T),
  caption = "Nombres de personnes par nombre d'agrégats de poste")

```

## Comparaison des EQTP

### Tous les sortants/entrants

Les entrants doivent normalement compléter la charge manquante après départs des
sortants sur l'année. Pour vérifier cela, on compare les EQTP des sortants et 
des entrants pour voir quelle est la somme globale en charge.

L'EQTP *complémentaire*, i.e. non réalisé après départ d'un sortant est calculé
comme `EQTP_COMPLEMENT = QUOTITE/100 - EQTP`. L'économie sur le salaire 
brut non payé après départ est calculée comme 
`S_BRUT_COMPLEMENT = S_BRUT/EQTP * EQTP_COMPLEMENT`.

*Remarque : Un problème restant est que certain EQTP agrégés sont supérieurs à 1.*


```{r infos_compar_eqtp, results='asis', echo=FALSE}

# Infos sortants
cat(sprintf("EQTP et salaires bruts des sortants (%d personnes)\n", nrow(df_out_ag)))

cat(sprintf("\n - Somme des EQTP : %.0f\n", sum(df_out_ag$EQTP)))
cat(sprintf(" - EQTP moyen réalisé SOMME_EQTP/NB_OUT = %.1f\n", sum(df_out_ag$EQTP)/nrow(df_out_ag)))
cat(sprintf(" - Estimation EQTP restant à faire : %.0f\n",
            sum(df_out_ag$EQTP_COMPLEMENT)))
cat(sprintf(" - Somme des S_BRUT : %.0f Millions €\n", sum(df_out_ag$S_BRUT)/1e6))
cat(sprintf(" - Estimation S_BRUT non fait : %.0f Millions €\n", sum(df_out_ag$S_BRUT_COMPLEMENT)/1e6))
cat(sprintf(" - S_BRUT/EQTP : %.1f\n", sum(df_out_ag$S_BRUT)/sum(df_out_ag$EQTP) ))

# Infos entrants
cat(sprintf("\nEQTP et salaires bruts des entrants (%d personnes)\n", nrow(df_in_ag)))

cat(sprintf("\n - Somme des EQTP : %.0f\n", sum(df_in_ag$EQTP)))
cat(sprintf(" - EQTP moyen réalisé : %.1f\n", sum(df_in_ag$EQTP)/nrow(df_in_ag)))
cat(sprintf(" - Somme des S_BRUT : %.0f Millions €\n", sum(df_in_ag$S_BRUT)/1e6))
cat(sprintf(" - S_BRUT/EQTP : %.1f\n", sum(df_in_ag$S_BRUT)/sum(df_in_ag$EQTP) ))

# Différences
cat("\nDeltas `sortants - entrants`\n")

cat(sprintf("\n - `S_BRUT non fait sortants - S_BRUT entrants` : %.0f Millions €\n", 
            (sum(df_out_ag$S_BRUT_COMPLEMENT) - sum(df_in_ag$S_BRUT))/1e6)
    )

```

### Comparaison par catégories A/B/C


```{r compar_in_out_cat, results='asis', echo=FALSE}

# Infos sortants

df_compar_cat <- data.frame(CATEGORIE = NULL, 
                            NB_OUT = NULL, 
                            NB_IN = NULL, 
                            EQTP_OUT = NULL, 
                            EQTP_OUT_COMPLEMENT = NULL,
                            EQTP_IN = NULL,
                            S_BRUT_OUT = NULL,
                            S_BRUT_OUT_COMPLEMENT = NULL,
                            S_BRUT_IN = NULL,
                            DELTA_S_BRUT = NULL)

for (categ in c("A", "B", "C")) {
  
  df_out_cat <- df_out_ag %>% filter(CAT_ECH == categ)
  df_in_cat <- df_in_ag %>% filter(CAT_ECH == categ)
  
  
  df_cat <- data.frame(CATEGORIE = categ, 
                            NB_OUT = nrow(df_out_cat), 
                            NB_IN = nrow(df_in_cat), 
                            EQTP_OUT = sum(df_out_cat$EQTP), 
                            EQTP_OUT_COMPLEMENT = sum(df_out_cat$EQTP_COMPLEMENT),
                            EQTP_IN = sum(df_in_cat$EQTP),
                            S_BRUT_OUT = sum(df_out_cat$S_BRUT),
                            S_BRUT_OUT_COMPLEMENT = sum(df_out_cat$S_BRUT_COMPLEMENT),
                            S_BRUT_IN = sum(df_in_cat$S_BRUT),
                            DELTA_S_BRUT = sum(df_out_cat$S_BRUT_COMPLEMENT) -
                         sum(df_in_cat$S_BRUT) )
  
  cat("\n#### Catégorie", categ, "\n\n")
  cat(sprintf("EQTP et salaires bruts des sortants (%d personnes)\n", df_cat$NB_OUT))
  cat(sprintf("\n - Somme des EQTP : %.0f\n", df_cat$EQTP_OUT))
  cat(sprintf(" - EQTP moyen réalisé SOMME_EQTP/NB_OUT = %.1f\n", 
              df_cat$EQTP_OUT/df_cat$NB_OUT))
  cat(sprintf(" - Estimation EQTP restant à faire : %.0f\n",
              df_cat$EQTP_COMPLEMENT))
  
  cat(sprintf(" - Somme des S_BRUT : %.0f Millions €\n", df_cat$S_BRUT_OUT/1e6))
  cat(sprintf(" - Estimation S_BRUT non fait : %.0f Millions €\n",
              sum(df_cat$S_BRUT_OUT_COMPLEMENT)/1e6))
  cat(sprintf(" - S_BRUT/EQTP : %.1f\n", df_cat$S_BRUT_OUT/df_cat$EQTP_OUT ))
  
  # Infos entrants
  cat(sprintf("\nEQTP et salaires bruts des entrants (%d personnes)\n", df_cat$NB_IN))
  
  cat(sprintf("\n - Somme des EQTP : %.0f\n", df_cat$EQTP_IN))
  cat(sprintf(" - EQTP moyen réalisé : %.1f\n", df_cat$EQTP_IN/df_cat$NB_IN))
  cat(sprintf(" - Somme des S_BRUT : %.0f Millions €\n", df_cat$S_BRUT_IN/1e6))
  cat(sprintf(" - S_BRUT/EQTP : %.1f\n", df_cat$S_BRUT_IN/df_cat$EQTP_IN ))
  
  # Différences
  cat(sprintf(paste("\nDelta `S_BRUT non fait sortants - S_BRUT entrants` : ",
                    "%.0f Millions €\n"),
             (df_cat$S_BRUT_OUT_COMPLEMENT - df_cat$S_BRUT_IN)/1e6) )

  # Concaténation 
  df_compar_cat <- rbind(df_compar_cat, df_cat)
}

```

```{r}

knitr::kable(df_compar_cat, 
             caption = sprintf("Comparaison entrants sortants %d par catégorie", annee)
             )

```


## Bilan entrants/sortants par établissement

### Bilan tous sortants/entrants

Pour approcher le calcul du GVT négatif, nous cherchons un appariement entre les 
sortants et les entrants. 

L'idée proposée ici est de regarder par `SIREN` le bilan : 

- du nombre de sortants et du nombre d'entrants
- de l'`EQTP` des entrants avec l'`EQTP_COMPLEMENT` des sortants
- du `S_BRUT` des entrants avec le `S_BRUT_COMPLEMENT` des sortants

```{r lien_in_out_etab}

# Regroupement sur le SIREN
df_siren_out <- df_out_ag %>% group_by(SIREN) %>% 
  summarise(SIREN = first(SIREN), 
            NB = n(), 
            EQTP = sum(EQTP), 
            EQTP_COMPLEMENT = sum(EQTP_COMPLEMENT), 
            S_BRUT = sum(S_BRUT),
            S_BRUT_COMPLEMENT = sum(S_BRUT_COMPLEMENT)
            )
df_siren_in <- df_in_ag %>% group_by(SIREN) %>% 
  summarise(SIREN = first(SIREN), 
            NB = n(), 
            EQTP = sum(EQTP), 
            S_BRUT = sum(S_BRUT)
            )

df_info_siren <- merge.data.frame(df_siren_out, df_siren_in,
                                 by = "SIREN", all = T, 
                                 suffixes = c("_OUT", "_IN")) %>% 
  arrange(desc(NB_OUT))


# Fix NA
df_info_siren[is.na(df_info_siren)] <- 0

knitr::kable(head(df_info_siren,20), 
             caption = "Synthèse sortants / entrants par établissement (extrait)")

```

```{r info_synt_etab, results='asis', echo=FALSE}

# Infos sortants

cat_bilan_etab <- function(df_bilan, strInfo) {
  cat(sprintf("\nBilan d'équilibre sortants / entrants sur les %d établissements (par %s)\n", 
              nrow(df_bilan), strInfo))
  
  df_tension <- df_bilan %>% filter(EQTP_IN < EQTP_COMPLEMENT)
  cat(sprintf("\n - %d ont un EQTP entrants **inférieur** au complément des sortants, dont :\n", 
              nrow(df_tension)))
  
  cat(sprintf("   - %d ont moins d'entrants que de sortants\n", 
              sum(df_tension$NB_IN < df_tension$NB_OUT)))

  cat(sprintf("   - %d ont autant d'entrants que de sortants\n", 
            sum(df_tension$NB_IN == df_tension$NB_OUT)))
  
  cat(sprintf("   - %d ont plus d'entrants que de sortants\n", 
              sum(df_tension$NB_IN > df_tension$NB_OUT)))
  
  cat(sprintf(" - %d ont un EQTP entrants **dépassant** le complément des sortants\n", 
              sum(df_bilan$EQTP_IN > df_bilan$EQTP_COMPLEMENT)))
  
  cat(sprintf(" - %d sont à l'**équilibre** EQTP entrants / complément sortants\n", 
              sum(df_bilan$EQTP_IN == df_bilan$EQTP_COMPLEMENT)))
  
  cat(sprintf(" - %d ont **plus** d'entrants que de sortants\n", 
              sum(df_bilan$NB_IN > df_bilan$NB_OUT)))
  
  cat(sprintf(" - %d ont **autant** d'entrants que de sortants\n", 
              sum(df_bilan$NB_IN == df_bilan$NB_OUT)))
  
  cat(sprintf(" - %d ont **moins** d'entrants que de sortants\n", 
              sum(df_bilan$NB_IN < df_bilan$NB_OUT)))
  
}

# 
cat_bilan_etab(df_info_siren, "SIREN")

```

### Bilan sans les contrats d'été


```{r}
df_in_ag_horsete <- df_in_ag %>% 
  filter(!(DAT_FIN <= as.POSIXlt(sprintf("%s-10-01", strAn), tz = "UTC") &
           DAT_DEBUT >= as.POSIXlt(sprintf("%s-06-01", strAn), tz = "UTC")  ))


nb_contratsete <- nrow(df_in_ag)-nrow(df_in_ag_horsete)
```

Nous refaisons le bilan en supprimant les entrants correspondant à un contrats 
dit d'été, c'est-à-dire les entrants à partir du 1 juin et sortant avant le 1er 
octobre, soit `r nb_contratsete` personnes 
(`r round(100* nb_contratsete/nrow(df_in_ag), digits = 1)` %).

```{r bilan_etab_horsete, results='asis'}
# Regroupement sur le SIREN
df_siren_in_horsete <- df_in_ag_horsete %>% group_by(SIREN) %>% 
  summarise(SIREN = first(SIREN), 
            NB = n(), 
            EQTP = sum(EQTP), 
            S_BRUT = sum(S_BRUT)
            )

df_info_siren_horsete <- merge.data.frame(df_siren_out, df_siren_in_horsete,
                                 by = "SIREN", all = T, 
                                 suffixes = c("_OUT", "_IN")) %>% 
  arrange(desc(NB_OUT))


# Fix NA
df_info_siren_horsete[is.na(df_info_siren_horsete)] <- 0

cat_bilan_etab(df_info_siren_horsete, "SIREN hors été")

```

### Bilan sans les contrats courts

```{r}
df_in_ag$DUREE_JOURS <- difftime(df_in_ag$DAT_FIN, df_in_ag$DAT_DEBUT, units="days")
df_in_ag_horscourt <- df_in_ag %>% 
  filter(!( DUREE_JOURS<=90 & 
  DAT_FIN < as.POSIXlt(sprintf("%s-12-15", strAn), tz = "UTC") ) ) 
         

nb_contratscourts <- nrow(df_in_ag)-nrow(df_in_ag_horscourt)

```

Nous refaisons le bilan en supprimant les entrants correspondant à un contrat 
dit *court*, défini comme des contrats de moins de 90 jours qui se terminent 
avant le 1er décembre, soit `r nb_contratscourts` personnes 
(`r round(100* nb_contratscourts/nrow(df_in_ag), digits = 1)` %).

```{r bilan_etab_horscourt, results='asis'}
# Regroupement sur le SIREN
df_siren_in_horscourt <- df_in_ag_horscourt %>% group_by(SIREN) %>% 
  summarise(SIREN = first(SIREN), 
            NB = n(), 
            EQTP = sum(EQTP), 
            S_BRUT = sum(S_BRUT)
            )

df_info_siren_horscourt <- merge.data.frame(df_siren_out, df_siren_in_horscourt,
                                 by = "SIREN", all = T, 
                                 suffixes = c("_OUT", "_IN")) %>% 
  arrange(desc(NB_OUT))


# Fix NA
df_info_siren_horscourt[is.na(df_info_siren_horscourt)] <- 0

cat_bilan_etab(df_info_siren_horscourt, "SIREN hors contrats courts")

```

## Comparaison des dates de sorties / entrées

### Sortants au 31/12 et entrants au 01/01 inclus 

La comparaison des histogrammes des fins de contrats des sortants et de début 
des entrants cherche à explorer une indication de causalité du type *"est-ce que les 
entrants remplacement majoritairement des sortants"*.

On voit d'une part que la majorité des fins de contrats est calée sur l'année 
civile, avec une fin au 31/12,le reste des fins de contrats se répartissant 
assez uniformément sur l'année. On n'observe pas de sortie majoritaire au 1er 
juillet.


```{r hist_finsortant_debentrants, echo=FALSE, message=FALSE, warning=FALSE}

x_lim <- c(as.Date(sprintf("%s-12-25", strAnPrec)), 
           as.Date(sprintf("%s-01-07", strAnSuiv)))
           
# Version ggplot
plot1 <- ggplot(df_out, aes(x=as.Date(DAT_FIN))) +  
  geom_histogram(binwidth=7, boundary=0) +
  labs(x="Date", y="Fréquence",
       title = sprintf("Histogrammes des dates fin contrat sortants %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot2 <- ggplot(df_in, aes(x=as.Date(DAT_DEBUT))) +  
  geom_histogram(binwidth=7, boundary=0) + # bins = 53) + # 
  labs(x="Date", y="Fréquence",
       title = sprintf("Dates début contrat entrants %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot1 / plot2

```

```{r hist_finsortant_debentrants_tith, echo=FALSE, message=FALSE, warning=FALSE}

# Version ggplot
plot1 <- ggplot(df_out %>% filter(STATUT_CONTRAT=="TITH"), aes(x=as.Date(DAT_FIN))) +  
  geom_histogram(binwidth=7, boundary=0) +
  labs(x="Date", y="Fréquence",
       title = sprintf("Histogramme des dates fin contrat sortants TITH %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot2 <- ggplot(df_in %>% filter(STATUT_CONTRAT=="TITH"), aes(x=as.Date(DAT_DEBUT))) +  
  geom_histogram(binwidth=7, boundary=0) + # bins = 53) + # 
  labs(x="Date", y="Fréquence",
       title = sprintf("Dates début contrat entrants TITH %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot1 / plot2

```

```{r hist_finsortant_debentrants_cont, echo=FALSE, message=FALSE, warning=FALSE}

# Version ggplot
plot1 <- ggplot(df_out %>% filter(STATUT_CONTRAT=="CONT"), aes(x=as.Date(DAT_FIN))) +  
  geom_histogram(binwidth=7, boundary=0) +
  labs(x="Date", y="Fréquence",
       title = sprintf("Histogramme des dates fin contrat sortants CONT %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot2 <- ggplot(df_in %>% filter(STATUT_CONTRAT=="CONT"), aes(x=as.Date(DAT_DEBUT))) +  
  geom_histogram(binwidth=7, boundary=0) + # bins = 53) + # 
  labs(x="Date", y="Fréquence",
       title = sprintf("Dates début contrat entrants CONT %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot1 / plot2

```

### Sortants au 31/12 et entrants au 01/01 exclus


```{r hist_finsortant_debentrants_lim, echo=FALSE, message=FALSE, warning=FALSE}

x_lim <- c(as.Date(sprintf("%s-12-25", strAnPrec)), 
           as.Date(sprintf("%s-01-07", strAnSuiv)))
           
# Version ggplot
plot1 <- ggplot(df_out %>% 
                  filter(DAT_FIN < as.POSIXlt(sprintf("%s-12-31", strAn), tz = "UTC")),
                aes(x=as.Date(DAT_FIN))) +  
  geom_histogram(binwidth=7, boundary=0) +
  labs(x="Date", y="Fréquence",
       title = sprintf("Histogrammes des dates fin contrat sortants %s, hors 31/12", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot2 <- ggplot(df_in %>% 
                  filter(DAT_DEBUT > as.POSIXlt(sprintf("%s-01-01", strAn), tz = "UTC")), aes(x=as.Date(DAT_DEBUT))) +  
  geom_histogram(binwidth=7, boundary=0) + # bins = 53) + # 
  labs(x="Date", y="Fréquence",
       title = sprintf("Dates début contrat entrants %s, hors 01/01", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot1 / plot2

```


## Dates de fin de contrats

La comparaison des histogrammes des fins de contrats pour l'ensemble des 
entrants `r strAn`` et ceux qui sortent la même année montre une quantité plus 
importante en été, qui indique vraisemblablement le recours aux contractuels 
pendant les grandes vacances. 

```{r hist_fin_contrat, echo=FALSE, message=FALSE, warning=FALSE}

# Version ggplot
plot1 <- ggplot(df_in, aes(x=as.Date(DAT_FIN))) +  
  geom_histogram(binwidth=7, boundary=0) +
  labs(x="Date", y="Fréquence",
       title = sprintf("Histogrammes des dates fin contrat entrants %s", strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot2 <- ggplot(df_inout, aes(x=as.Date(DAT_FIN))) +  
  geom_histogram(binwidth=7, boundary=0) + # bins = 53) + # 
  labs(x="Date", y="Fréquence",
       title = sprintf("Dates fin contrat entrants %s sortis en %s", strAn, strAn)) + 
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  scale_x_date(date_breaks = "month", date_labels = "%m-%Y", limits = x_lim) 

plot1 / plot2

```

## Durées de contrats

Les histogrammes des *durées de contrats* pour les entrants `r strAn` mettent 
en évidence plusieurs tendances : 

- Les entrants `r strAn` qui restent en `r strAnSuiv` ont une durée de contrat qui passe 
majoritairement à temps plein 
- Les entrants `r strAn` qui ne restent pas ont des contrats majoritairement 
inférieurs à 3 mois.


```{r hist_duree_contrat, echo=FALSE, message=FALSE, warning=FALSE}

# Version ggplot
day_ticks = c(seq(0,300, 50), 365)
plot1 <- ggplot(NULL, aes(x=as.numeric(df_in$DAT_FIN - df_in$DAT_DEBUT) / (24*60*60))) +  
  geom_histogram(binwidth = 20, boundary = 0) +
  labs(x="Jours", y="Fréquence", 
       title = sprintf("Histogrammes des durées de contrats entrants %s en %s", 
                       strAn, strAn)) + 
  scale_x_continuous(limits = c(0,380), breaks = day_ticks)

plot2 <- ggplot(NULL, aes(x=as.numeric(df_in_anSuiv$DAT_FIN - df_in_anSuiv$DAT_DEBUT) / (24*60*60))) +  
  geom_histogram(binwidth = 20, boundary = 0) +
  labs(x="Jours", y="Fréquence",
       title = sprintf("Durée des contrats entrants %s en %s", strAn, strAnSuiv)) + 
  scale_x_continuous(limits = c(0,380), breaks = day_ticks)

plot3 <- ggplot(NULL, aes(x=as.numeric(df_inout$DAT_FIN - df_inout$DAT_DEBUT) / 
                            (24*60*60))) +  
  geom_histogram(binwidth = 20, boundary = 0) +
  labs(x="Jours", y="Fréquence", 
       title = sprintf("Durée des contrats entrants %s sortis en %s", strAn, strAn)) + 
  scale_x_continuous(limits = c(0,380), breaks = day_ticks)

plot1 / plot2 / plot3

```

## EQTP 

Nous vérifions les hypothèses précédentes sur les histogrammes des EQTP qui 
montrent les mêmes tendances que les durées de contrats.

```{r hist_eqtp, echo=FALSE}

plot1 <- ggplot(df_in, aes(x = EQTP)) +  
  geom_histogram(binwidth = 1/20, center = 1/40, colour = "white") +
  labs(x="EQTP", y="Fréquence", 
       title = sprintf("Histogramme des EQTP entrants %s en %s", strAn, strAn))

plot2 <- ggplot(df_in_anSuiv, aes(x = EQTP)) +  
  geom_histogram(binwidth = 1/20, center = 1/40, colour = "white") +
  labs(x="EQTP", y="Fréquence", 
       title = sprintf("EQTP entrants %s en %s", strAn, strAnSuiv))

plot3 <- ggplot(df_inout, aes(x = EQTP)) +  
  geom_histogram(binwidth = 1/20, center = 1/40, colour = "white") + # ) + # 
  labs(x="EQTP", y="Fréquence", 
       title = sprintf("EQTP entrants %s sortis en %s", strAn, strAn) )

plot1 / plot2 / plot3

```

# Analyse des périodes travaillées

## Par type d'entrants

La figure ci-dessous propose une visualisation des périodes d'emploi des 
différents types d'emploi. Pour cela, on discrétise l'année en jour 
et on comptabilise pour chaque contrat les jours travaillés à partir de 
la date de début.

La figure peut donc être interprétée comme un _histogramme de fréquentation_ 
des entrants. 

```{r hist_cumul_duree_contrat,warning=FALSE, echo=FALSE}
hist_duree_contrat <- function(df, str_titre) {
  # En jour 
  v_jour_deb <- as.integer(format(df$DAT_DEBUT, "%j")) # En jours
  v_jour_fin <- as.integer(format(df$DAT_FIN, "%j"))   # En jours
  v_bins <- vector(mode = "integer", length = 370)
  lab = "jours"
  xticks = cumsum(c(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,31))
  xlabels = c('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
              'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre',
              'Décembre', 'Janvier')

  for (idx in seq(1,nrow(df))) {
    v_idx <- v_jour_deb[idx]:v_jour_fin[idx]
    v_bins[v_idx] <- v_bins[v_idx] + 1
  }
  
  # Version GGplot
  fig <- ggplot(data = NULL, aes(x = seq(1,length(v_bins)), y = v_bins)) + 
    geom_line() +
    labs(x="Jours", y="Cumul", title = str_titre) + 
    theme(axis.text.x=element_text(angle=45, hjust=1)) +
    scale_x_continuous(breaks = xticks, labels = xlabels, limits = c(0,max(xticks)+2))
  
  invisible(fig)
}

# par(mfrow=c(3,1))
plot1 <- hist_duree_contrat(df_in, 
                   sprintf("Cumul des jours travaillés en %s, entrants %s", 
                           strAn,strAn ))
plot2 <- hist_duree_contrat(df_inout, 
                    sprintf( "Cumul des jours travaillés en %s, entrants %s sortis en %s",
                             strAn, strAn, strAn))

plot3 <- hist_duree_contrat(df_in_anSuiv,
                            sprintf("Cumul des jours travaillés en %s, entrants %s",
                                    strAnSuiv, strAn))

plot1 / plot2
print(plot3)

# plot1 / plot2 / plot3
```

## Par types de contrats

On sépare cette fois par type de contrat : TITH, CONT, MEDI, ... 

```{r cumul_duree_statut, echo=FALSE, message=FALSE}
calculerHistDureeContrat <- function(df) {
  # En jour 
  df$JOUR_DEB <- as.integer(format(df$DAT_DEBUT, "%j")) # En jours
  df$JOUR_FIN <- as.integer(format(df$DAT_FIN, "%j")) # En jours
  v_bins <- vector(mode = "integer", length = 370)
  xlab = "jours"
  xticks = cumsum(c(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,31))
  xlabels = c('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
              'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre',
              'Décembre', 'Janvier')

  v_statuts <- unique(df$STATUT_CONTRAT)
  df_out <- data.frame(x = seq(1,length(v_bins)))
  for (statut in v_statuts) {
    v_bins <- vector(mode = "integer", length = 370)
    df_statut <- df %>% filter(STATUT_CONTRAT == statut)
    for (idx in seq(1,nrow(df_statut))) {
      v_idx <- (df_statut$JOUR_DEB[idx]:df_statut$JOUR_FIN[idx])
      v_bins[v_idx] <- v_bins[v_idx] + 1
    }
    df_out[, statut] <- v_bins
  }

  return(list( df=df_out, xticks = xticks, xlabels = xlabels))
}

# "CONT" "MEDI" "TITH" "CAID" "ELEV" "ACTE" "SCIV" "INTE"
# "AUTR" "APPR" "ASMA" "TITT" "OUVR" "TITE" "PACT"
hist_in <- calculerHistDureeContrat(df_in)

ggplot(data = hist_in$df, aes(x = x)) + 
  geom_line(aes(y = TITH, colour = "TITH")) + 
  geom_line(aes(y = CONT, colour = "CONT")) + 
  geom_line(aes(y = MEDI, colour = "MEDI")) + 
  geom_line(aes(y = CAID, colour = "CAID")) + 
  labs(x = "Jours", y = "Cumul", 
       title = sprintf("Cumul des jours travaillés en %s, entrants %s", strAn, strAn)) + 
    theme(axis.text.x=element_text(angle=30, hjust=1))  + 
  scale_x_continuous(breaks = hist_in$xticks, labels = hist_in$xlabels)
   # , limits = c(0,max(hist_in_out$xticks)+2))


hist_in_out <- calculerHistDureeContrat(df_inout)

ggplot(data = hist_in_out$df, aes(x = x)) + 
  geom_line(aes(y = TITH, colour = "TITH")) + 
  geom_line(aes(y = CONT, colour = "CONT")) + 
  geom_line(aes(y = MEDI, colour = "MEDI")) + 
  geom_line(aes(y = CAID, colour = "CAID")) + 
  labs(x = "Jours", y = "Cumul", 
       title = sprintf("Cumul des jours travaillés en %s, entrants %s sortis en %s", 
                       strAn, strAn, strAn)) + 
    theme(axis.text.x=element_text(angle=30, hjust=1))  +
  scale_x_continuous(breaks = hist_in_out$xticks, labels = hist_in_out$xlabels)
    # , limits = c(0,max(hist_in_out$xticks)+2))

hist_in_anSuiv <- calculerHistDureeContrat(df_in_anSuiv)

ggplot(data = hist_in_anSuiv$df, aes(x = x)) + 
  geom_line(aes(y = TITH, colour = "TITH")) + 
  geom_line(aes(y = CONT, colour = "CONT")) + 
  geom_line(aes(y = MEDI, colour = "MEDI")) + 
  geom_line(aes(y = CAID, colour = "CAID")) + 
  labs(x = "Jours", y = "Cumul", 
       title = sprintf("Cumul des jours travaillés en %s, entrants %s", strAnSuiv, strAn)) + 
    theme(axis.text.x=element_text(angle=30, hjust=1)) + 
  scale_x_continuous(breaks = hist_in_anSuiv$xticks, labels = hist_in_anSuiv$xlabels)


```


# Évolution des contrats sortants

## EQTP des sortants l'année précédente,  `r strAnPrec`

Nous comparons les EQTP des sortants présents en `r strAnPrec` et en `r strAn`.
Il apparaît que le temps de travail des sortants diminue drastiquement l'année 
où ils sortent, ce qui n'est pas tout à fait cohérent avec les dates de fin 
contrat annoncées.


```{r evolution_sortant, echo=FALSE}
# Filtre sur id_out_anPrec
plot1 <- ggplot(df_out %>% filter(ID_NIR20 %in% id_out_anPrec), aes(x = EQTP)) +  
  geom_histogram(binwidth = 1/20, center = 1/40, colour = "white") +
  labs(x="EQTP", y="Fréquence", 
       title = sprintf("Histogrammes des EQTP %s des sortants %s présents en %s", 
                       strAn, strAn, strAnPrec))

plot2 <- ggplot(df_out_anPrec, aes(x = EQTP)) +
  geom_histogram(binwidth = 1/20, center = 1/40, colour = "white") +
  labs(x="EQTP", 
       y="Fréquence",
       title = sprintf("EQTP %s, 1 an avant leur sortie, des sortants %s", 
                       strAnPrec, strAn))

plot2 / plot1

```

## Delta EQTP entre `r strAnPrec` et `r strAn`

Ci-dessous l'histogramme des différences d'EQTP pour chaque sortant après 
agrégat en sommant les EQTP de chaque contrat. 

On observe un pic en 0, et une tendance à la diminution d'activité (Delta>0)

```{r diff_eqtp_sortant, echo=FALSE}

# Agrégat des EQTP par Id
eqtp_anPrec <- df_out_anPrec %>%  group_by(ID_NIR20) %>% 
  summarise(EQTP = sum(EQTP)) %>% arrange(ID_NIR20)

eqtp_an <- df_out %>% filter(ID_NIR20 %in% id_out_anPrec) %>%  
  group_by(ID_NIR20) %>% summarise(EQTP = sum(EQTP)) %>% arrange(ID_NIR20)


plot1 <- ggplot(NULL, aes(x = eqtp_anPrec$EQTP - eqtp_an$EQTP)) +  
  geom_histogram(binwidth = 0.1, colour = "white") +
  labs(x="Delta EQTP", y="Fréquence", 
       title = paste("Histogramme delta EQTP", strAnPrec, "-", strAn, 
       "des sortants", strAn, "présents en", strAnPrec))

plot1

```

