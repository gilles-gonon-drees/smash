
# Test synthèse de population par IPF ----

library(synthpop)
devtools::load_all("../smash-r-pkg/")

an <- 2018
df_an <- getEchantillonSIASP("hop", an, 0)
df_in <- df_an %>% filter(IS_ENTRANT)

df_in_3021 <- df_in %>% filter(GRADE == "3021")

l_cols <- c("AGE", "STATUT_CONTRAT", "DUREE_CONTRAT", "EQTP", 
            "QUOTITE", "S_BRUT", "SEXE")

popsynt <- syn(df_in_3021 %>% select(all_of(l_cols)), k = 1000)

df_s_d <- popsynt$syn %>% 
  count(STATUT_CONTRAT, DUREE_CONTRAT, sort = T) %>% 
  mutate(PCT = round(100*n/sum(n), digits = 2))

df_s_d_2 <- df_in_3021 %>% 
  count(STATUT_CONTRAT, DUREE_CONTRAT, sort = T) %>% 
  mutate(PCT = round(100*n/sum(n), digits = 2))
