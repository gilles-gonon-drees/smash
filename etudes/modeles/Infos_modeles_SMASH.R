# Infos des modèles utilisés dans SMASH ----
# Ce script charge les modèles utilisées dans les simulations SMASH
# afin d'explorer facilement les varaibles contenues dans les fichiers RDS

# Liste des modèles utilisés ----
# Chemin vers les modèles
dir_model <- "T:/BPS/SMASH/modeles"
champ <- "hop"
anneeDebut <- 2017

# Entrants
model_entrants <- file.path(
  dir_model,
  sprintf("modele_entrants_v2_%s_%d.rds", champ, anneeDebut))

model_entrants_anplus1 <- file.path(
  dir_model,
  sprintf("modele_entrants_%s_anplus1_%d.rds",champ, anneeDebut))

# Sortants
model_sortants_anmoins1 <- file.path(
  dir_model,
  sprintf("modele_sortants_%s_anmoins1_%d.rds",champ, anneeDebut))
model_INSEE_mortalite  <- file.path(dir_model, "mortalite_Ensemble.rds")
model_retraite <- file.path(
  dir_model,
  sprintf("mdl_retraites_taux_%s_%d.rds",champ, anneeDebut) )

model_TauxEmpiriques_sortants <-
  file.path(dir_model,
            sprintf("modele_sortants_tauxempiriques_%s_%d.rds", champ, anneeDebut))

# Salaires
model_primes <- file.path(dir_model, "modele_primes.rds")
model_lm_pente_salaire_CONT <- file.path(dir_model, "pente_salaires_grade_CONT.rds")
model_lm_pente_salaire_TITH <- file.path(dir_model, "pente_salaires_grade_TITH.rds")
model_lm_pente_salaire_MEDI <- file.path(dir_model, "pente_salaires_grade_MEDI.rds")

# Transitions de grade (Promotion)
model_transitionGrade <-
  file.path(dir_model, "modele_transitionGrade_TableProbabilite.rds")
model_promotionGradeIM <- 
  file.path(dir_model, "mat_transition_grade_im_hop_2014-2020.rds")


# Transition de contrat (cdisation/titularisation)
model_prochain_contrat_tx <- 
  file.path(dir_model, "changement_statut_contractuels_grade.rds")

# Chargement des modèles ----
mdl_entrants <- readRDS(model_entrants)
mdl_entrants_anplus1 <- readRDS(model_entrants_anplus1)

mdl_sortants_anmoins1 <- readRDS(model_sortants_anmoins1)
mdl_INSEE_mortalite <- readRDS(model_INSEE_mortalite)
mdl_retraite <- readRDS(model_retraite)

mdl_TauxEmpiriques_sortants <- readRDS(model_TauxEmpiriques_sortants)
mdl_primes <- readRDS(model_primes)
mdl_lm_pente_salaire_CONT <- readRDS(model_lm_pente_salaire_CONT)
mdl_lm_pente_salaire_TITH <- readRDS(model_lm_pente_salaire_TITH)
mdl_lm_pente_salaire_MEDI <- readRDS(model_lm_pente_salaire_MEDI)

mdl_transitionGrade <- readRDS(model_transitionGrade)
mdl_promotionGradeIM <- readRDS(model_promotionGradeIM)
mdl_prochain_contrat_tx <- readRDS(model_prochain_contrat_tx)

# Infos modèles ----
# Si nécéssaire afficher les infos sur les modèles dans la console
print_info_mdl <- function(mdl) {
  cat(" - ", paste(names(mdl), collapse, "\n - "), "\n")
}


